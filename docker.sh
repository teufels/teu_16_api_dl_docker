#!/bin/bash

#######################################
## teufels-creator
#######################################

version="v.0.0.4";
docker_sync__container_placeholder="XXX_yy_xx_xxxx" 
installer__t_boilerplate_placeholder="t_boilerplate_YY-mm-dd_HH-MM-SS"

init() {

    if [ ! -f docker.ini ]; then
        errorMsg "Error in 'Project' configuration\n" \
        ":: File 'docker.ini' does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #include docker.ini variables
    . docker.ini

    infoMsg "::\n" \
    ":: Variables:\n" \
    ":: docker__dockerfile: $docker__dockerfile\n" \
    ":: docker_sync__container: $docker_sync__container\n" \
    "::\n"

    FILE="./$docker__dockerfile"
    if [ ! -f $FILE ]; then
        errorMsg ":: [34aa56ce9a9b30b3be59fc36d773295b]\n" \
        ":: Error in 'docker.ini' configuration\n" \
        ":: File '$docker__dockerfile' does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #include ../t_installer/installer.ini variables
    . ../t_installer/install.ini

    FILE="../t_installer/install.ini"
    if [ ! -f $FILE ]; then
        errorMsg ":: [a011525ae58805e36cc6ece9f8ed3d68]\n" \
        ":: Error in 'install.ini' configuration\n" \
        ":: File <../t_installer/install.ini> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$t_boilerplate" = "$installer__t_boilerplate_placeholder" ]; then
        errorMsg ":: [ac86811491a1dccf7f3fb8969dec9fc8]\n" \
        ":: Error in 'install.ini' configuration\n" \
        ":: Variable '$t_boilerplate' must not be '$t_boilerplate'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ -z "$t_boilerplate" ]; then
        errorMsg ":: [d8de8e7e50183029ebd1d3ebe3e5a030]\n" \
        ":: Error in 'install.ini' configuration\n" \
        ":: Variable '$t_boilerplate' is empty\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

}

plain() {

    #include docker.ini variables
    . docker.ini

    #include ../t_installer/installer.ini variables
    . ../t_installer/install.ini

    cp ./docker-compose.v2.yml ./docker-compose.yml
    sed -ie "s/###dockerfile###/$docker__dockerfile/g" ./docker-compose.yml
    #rm ./docker-compose.ymle


    infoMsg "::\n" \
    ":: diff docker-compose.v2.yml docker-compose.yml\n" \
    "::"

    infoMsg "+"
    diff -by -W100 --suppress-common-lines ./docker-compose.v2.yml  ./docker-compose.yml
    infoMsg "+"
    nbsp

    ##############################################
    # Copy files to boilerplate
    ##############################################
    cp -rv {docker-compose.yml,Dockerfile.php7teufels,Dockerfile.teufels,Makefile.teufels,teufels} ../"$t_boilerplate"/
    FILE="../$t_boilerplate/docker-sync.yml"
    if [ -f $FILE ]; then
        rm ../"$t_boilerplate"/docker-sync.yml
    fi
    
}

sync_formac() {
    
    #include docker.ini variables
    . docker.ini

    #include ../t_installer/installer.ini variables
    . ../t_installer/install.ini
    #use $t_boilerplate

    ## 0
    ## Check name for docker-sync container

    if [ "$docker_sync__container" = "$docker_sync__container_placeholder" ]; then
        errorMsg ":: [d12b241f7d83a0558f5620ea6a89acd9]\n" \
        ":: Error in configuration\n" \
        ":: \$docker_sync__container must not be '$docker_sync__container'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    ## 1
    ## prepare <docker-compose.yml>

    cp ./docker-compose.v2.sync.dockerformac.yml ./docker-compose.yml

    ## 1.1 ##
    ## replace '###dockerfile###' in <docker-compose.yml>

    sed -ie "s/###dockerfile###/$docker__dockerfile/" ./docker-compose.yml
    #rm ./docker-compose.ymle

    infoMsg "::\n" \
    ":: diff docker-compose.v2.sync.dockerformac.yml docker-compose.yml\n" \
    "::"

    infoMsg "+"
    diff -by -W100 --suppress-common-lines docker-compose.v2.sync.dockerformac.yml  docker-compose.yml
    infoMsg "+"
    nbsp

    ## 1.2
    ## replace '###sync###' in <docker-compose.yml>

    sed -ie "s/###sync###/$docker_sync__container/" ./docker-compose.yml
    #rm ./docker-compose.ymle

    infoMsg "::"
    infoMsg ":: diff ./docker-compose.v2.sync.dockerformac.yml ./docker-compose.yml"
    infoMsg "::"
    nbsp
    infoMsg "--"
    diff -by -W100 --suppress-common-lines ./docker-compose.v2.sync.dockertoolbox.yml ./docker-compose.yml
    infoMsg "--"
    nbsp

    ## 2 ##
    ## replace '###sync###' in <docker-sync.yml>

    cp ./docker-sync.dockerformac.yml ./docker-sync.yml
    sed -ie "s/###sync###/$docker_sync__container/" ./docker-sync.yml
    #rm ./docker-sync.ymle

    infoMsg "::"
    infoMsg ":: diff ./docker-sync.dockerformac.yml ./docker-sync.yml"
    infoMsg "::"
    nbsp
    infoMsg "+-"
    diff -by -W100 --suppress-common-lines ./docker-sync.dockerformac.yml ./docker-sync.yml
    infoMsg "-+"
    nbsp

    ##############################################
    # Copy files to boilerplate
    ##############################################
    cp -rv {docker-compose.yml,docker-sync.yml,Dockerfile.php7teufels,Dockerfile.teufels,Makefile.teufels,teufels} ../"$t_boilerplate"/

}

sync_fortoolbox() {

    #include docker.ini variables
    . docker.ini

    #include ../t_installer/installer.ini variables
    . ../t_installer/install.ini
    #use $t_boilerplate

    ## 0
    ## Check name for docker-sync container

    if [ "$docker_sync__container" = "$docker_sync__container_placeholder" ]; then
        errorMsg ":: [d12b241f7d83a0558f5620ea6a89acd9]\n" \
        ":: Error in configuration\n" \
        ":: \$docker_sync__container must not be '$docker_sync__container'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    ## 1 ##
    ## prepare <docker-compose.yml>

    cp ./docker-compose.v2.sync.dockertoolbox.yml ./docker-compose.yml

    ## 1.1 ##
    ## replace '###dockerfile###' in <docker-compose.yml>

    sed -ie "s/###dockerfile###/$docker__dockerfile/" ./docker-compose.yml
    #rm ./docker-compose.ymle

    infoMsg "::\n" \
    ":: diff ./docker-compose.v2.sync.dockertoolbox.yml ./docker-compose.yml\n" \
    "::"

    infoMsg "+"
    diff -by -W100 --suppress-common-lines ./docker-compose.v2.sync.dockertoolbox.yml ./docker-compose.yml
    infoMsg "+"
    nbsp

    ## 1.2
    ## replace '###sync###' in <docker-compose.yml>

    sed -ie "s/###sync###/$docker_sync__container/" ./docker-compose.yml
    #rm ./docker-compose.ymle

    infoMsg "::"
    infoMsg ":: diff ./docker-compose.v2.sync.dockertoolbox.yml ./docker-compose.yml"
    infoMsg "::"
    nbsp
    infoMsg "--"
    diff -by -W100 --suppress-common-lines ./docker-compose.v2.sync.dockertoolbox.yml ./docker-compose.yml
    infoMsg "--"
    nbsp

    ## 2 ##
    ## replace '###sync###' in <docker-sync.yml>

    cp ./docker-sync.dockertoolbox.yml ./docker-sync.yml
    sed -ie "s/###sync###/$docker_sync__container/" ./docker-sync.yml
    #rm ./docker-sync.ymle

    infoMsg "::"
    infoMsg ":: diff docker-sync.dockertoolbox.yml docker-sync.yml"
    infoMsg "::"
    nbsp
    infoMsg "+-"
    diff -by -W100 --suppress-common-lines docker-sync.dockertoolbox.yml docker-sync.yml
    infoMsg "-+"
    nbsp

    ##############################################
    # Copy files to boilerplate
    ##############################################
    cp -rv {docker-compose.yml,docker-sync.yml,Dockerfile.php7teufels,Dockerfile.teufels,Makefile.teufels,teufels} ../"$t_boilerplate"/
    
}

errorMsg() {
    echo -e "" \
    $(tput setaf 1)::$(tput sgr 0) "\n"\
    $(tput setaf 1)$*$(tput sgr 0) "\n"\
    $(tput setaf 1)::$(tput sgr 0)
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "$*"
}

nbsp() {
	echo ""
}

if [ "$#" -lt 1 ]; then
    errorMsg ":: Missing argument [a3ffba251c6974f261b35f4657a9ba17] \n" \
    ":: Specify 'plain', 'sync formac' or 'sync fortoolbox'\n" \
    ":: Feel free to read the manual by executing 'make help'"
    exit
fi

case "$1" in
    ###################################
    ## Docker
    ###################################
    "plain")
        init
        plain
        ;;

    ###################################
    ## Docker Sync
    ###################################
    "sync")
        if [ "$#" -lt 2 ]; then
            errorMsg ":: Missing argument for 'docker-sync' [58ea140d644dbf40a41e6a38c21b10fa] \n" \
            ":: Specify second argument 'formac' or 'fortoolbox'\n" \
            ":: Feel free to read the manual by executing 'make help'"
            exit
        fi
            case "$2" in
                ###################################
                ## docker-sync for Mac
                ###################################
                "formac")
                init
                sync_formac
                ;;

                ###################################
                ## docker-sync for Mac (toolbox)
                ###################################
                "fortoolbox")
                init
                sync_fortoolbox
                ;;

                ###################################
                ## Default
                ###################################
                *) 
                errorMsg ":: Wrong argument for 'docker-sync' [01ffa23315f06807ac45c884db8680e9]\n" \
                ":: Specify second argument 'formac' or 'fortoolbox'\n" \
                ":: Feel free to read the manual by executing 'make help'"
                ;;
            esac
        ;;

    ###################################
    ## Default
    ###################################
    *) 
        errorMsg ":: Wrong argument [6ba0ed25afcb12edadd37a54767870fd]\n" \
        ":: Specify 'plain', 'sync formac' or 'sync fortoolbox'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        ;;

esac

#TODO
# gitignore => ignore generated docker-compose and docker-sync files